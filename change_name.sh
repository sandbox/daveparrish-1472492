#!/bin/bash

# The purpose of this script is to change the name of the project to what you want.
# For example, change the name from 'devstart' to 'my_project' or 'upgradeya_dot_com'.

# Verify that the first parameter is present.
if [ -z "$1" ]
then
  echo "Parameter 1 missing: Must enter the project name name to change to."
  exit
fi

# Verify that the first parameter is alphanumeric
if ! grep -e '^\w*$' <<<$1 ;
  then 
  echo "Parameter 1 is not alphanumeric, please enter an alphanumeric name."
  exit
fi

# Get the current name which we will assume is the first part of the _distro.make file
# TODO: quite if there are more than one matches on *_distro.make
distro_files="*_distro.make"
regex="^(\w*)_distro.make$"
for f in $distro_files
do
  [[ $f =~ $regex ]]
  distro_name="${BASH_REMATCH[1]}"
done

# Rename all instances of name in makefile
sed -i -e "s/${distro_name}/$1/g" makefile 

# Rename the _distro.make file
mv ${distro_name}_distro.make $1_distro.make
