; Make file modeled from the build_kit make file found in the link below.
; http://drupalcode.org/project/buildkit.git/blob_plain/122d308ca2d103484b54958ca35e2797fb0215c0:/distro.make
;
; For more information visit the extending build kit instructions here:
; http://drupalcode.org/project/buildkit.git/blob/122d308ca2d103484b54958ca35e2797fb0215c0:/README.txt

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.15"

; Use vocabulary machine name for permissions
; http://drupal.org/node/995156
projects[drupal][patch][995156] = http://drupal.org/files/issues/995156-5_portable_taxonomy_permissions.patch

; Add devstart installation profile to the full Drupal distro build
projects[devstart][type] = profile
projects[devstart][download][type] = git
projects[devstart][download][url] = daveparrish@git.drupal.org:sandbox/daveparrish/1473098.git
