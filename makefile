# Makefile for building the Drupal distribution

# Get the backup folder
backupDir=.backups/$(shell date +%F-%s)

rebuild: clean build

# Gets the Drupal distribution
build:
	drush make --working-copy --prepare-install --no-gitinfofile devstart_distro.make httpdocs

# The purpose of this is mostly for updating Drupal core from the devstart_distro.make file
# But, this will also update all install profiles as well.  Database and files will be preserved.
update:
	drush make --working-copy --no-gitinfofile devstart_distro.make httpdocs_tmp # Build a new distro
	cp -r httpdocs/sites httpdocs_tmp # Copy the sites directory into the new distro and backup the old distro
	-chmod -R 777 httpdocs_bak
	mkdir -p $(backupDir)
	-mv httpdocs_bak $(backupDir) # It is safer to move than delete
	mv httpdocs httpdocs_bak
	mv httpdocs_tmp httpdocs
	cd httpdocs; drush updb # Update database, just in case

clean:
	mkdir -p $(backupDir)
	-mv httpdocs $(backupDir) # moving is safer than deleting!
	echo "You may want to delete your 'backup' folder from time to time, if it gets large."

# Sometimes things can get into the /sites/all directory like using "drush dl"
# We use clean-sites to make sure that nothing is in there
clean-sites-all:
	mkdir -p $(backupDir)/httpdocs/sites/all/themes
	-mv httpdocs/sites/all/themes/* $(backupDir)/httpdocs/sites/all/themes
	mkdir -p $(backupDir)/httpdocs/sites/all/modules
	-mv httpdocs/sites/all/modules/* $(backupDir)/httpdocs/sites/all/modules
